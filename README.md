# FEAR GARDEN - IOS GAME #

## Open Source ##

Fear Garden is an iOS Game using LevelSVG is an open source project that uses cocos2d-iphone and Box2D.

You can use all the LevelSVG source code as long as you comply with its license (MIT).


## Disclaimer ##

Although this source code is used to test cocos2d-iphone, parts of the code do not follow some of the new iOS / cocos2d-iphone best practices.
Use the source code at your own risk.

LevelSVG is using an outdated version of cocos2d-iphone.


https://bitbucket.org/firstpixel/feargarden/raw/f5dbb7f35ba079a7f97a4a73ecb894349c0fb563/Resources/movies/FearGarden.m4v
