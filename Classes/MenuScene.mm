//
//  MenuNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 22/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "MenuScene.h"
#import "SettingsScene.h"
#import "SelectLevelScene.h"
#import "AboutScene.h"
#import "SoundMenuItem.h"
#import "SimpleAudioEngine.h"
#import "ShoppingCartScene.h"
#import "MKStoreKit.h"
#import "SelectCharScene.h"
#import "IntroGameScene.h"
#import "GlobalSingleton.h"
#import "GameConstants.h"


@implementation MenuScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [MenuScene node];
	
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
        
        //audio
        if ([SimpleAudioEngine sharedEngine] != nil) {
            if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO){
                [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menuLoop.mp3"];
                if ([[SimpleAudioEngine sharedEngine] willPlayBackgroundMusic]) {
                    [[SimpleAudioEngine sharedEngine] setBackgroundMusicVolume:0.5f];
                }
                [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menuLoop.mp3" loop:YES];
            }
        }
        
		CGSize s = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background = [CCSprite spriteWithFile:@"main-menu-background.png"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"platforms.plist"];
        
        NSLog(@"IS_IPHONE %s", IS_IPHONE?"YES":"NO");
        NSLog(@"IS_IPAD %s", IS_IPAD?"YES":"NO");
        NSLog(@"IS_IPADHD %s", IS_IPADHD?"YES":"NO");
        NSLog(@"IS_RETINA %s", IS_RETINA?"YES":"NO");
        NSLog(@"IS_RETINA3X %s", IS_RETINA3X?"YES":"NO");
        NSLog(@"kPhysicsPTMRatio %f", kPhysicsPTMRatio);
        NSLog(@"CC_CONTENT_SCALE_FACTOR %f",CC_CONTENT_SCALE_FACTOR());
        
        
        if (IS_IPAD || IS_IPADHD || IS_RETINA) {
            
            [CCMenuItemFont setFontName:@"boingo"];
            [CCMenuItemFont setFontSize:64];
        
        }else{
            [CCMenuItemFont setFontName:@"boingo"];
            [CCMenuItemFont setFontSize:32];
        }
        
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
		
        CCMenu *menu = nil;
        
        
        if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.feargarden.full2"] == YES){
            
            CCMenuItemFont * item0 = [CCMenuItemFont itemWithString:@"Play" target:self selector:@selector(play:)];
            
            
            
            CCMenuItemFont * item1 = [CCMenuItemFont itemWithString:@"Intro" target:self selector:@selector(intro:)];
            
            CCMenuItemFont * item2 = [CCMenuItemFont itemWithString:@"About" target:self selector:@selector(about:)];
            
            menu = [CCMenu menuWithItems: item0, item1, item2,  nil];
            
		}else{
             
            
            CCMenuItemFont * item0 = [CCMenuItemFont itemWithString:@"Play" target:self selector:@selector(play:)];
            
            CCMenuItemFont * item1 = [CCMenuItemFont itemWithString:@"Intro" target:self selector:@selector(intro:)];
            
            CCMenuItemFont * item2 = [CCMenuItemFont itemWithString:@"Buy full version" target:self selector:@selector(shopping:)];
            
            CCMenuItemFont * item3 = [CCMenuItemFont itemWithString:@"About" target:self selector:@selector(about:)];
            
            menu = [CCMenu menuWithItems: item0, item1, item2, item3,  nil];
            
        }
        
		[menu alignItemsVertically];
        menu.position = ccp(s.width/2, s.height/2-30);
		
		[self addChild:menu];
        
	}
	
	return self;
    
}

-(void) play:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"gameIntro"]){
        [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[SelectCharScene scene]]];
    }else{
        [[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[IntroGameScene scene]]];
    }
}

-(void) intro:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[IntroGameScene scene]]];
}

-(void) settings:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SettingsScene scene] withColor:ccWHITE]];
}

-(void) about:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFlipY transitionWithDuration:1 scene:[AboutScene scene]]];
}

-(void) shopping:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFlipY transitionWithDuration:1 scene:[ShoppingCartScene scene]]];	
}

-(void) buyCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.firstpixel.com/"]];
}



@end
