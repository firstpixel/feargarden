//
//  HUD.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 16/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

//
// HUD: Head Up Display
//
// - Display score
// - Display lives
// - Display joystick, but it is not responsible for reading it
// - Display the menu button
// - Register a touch events: drags the screen
//

#import "HUD.h"
#import "GameConfiguration.h"
#import "GlobalSingleton.h"
#import "Joystick.h"
#import "JoystickCar.h"
#import "GameNode.h"
#import "MenuScene.h"
#import "SelectLevelScene.h"
#import "Hero.h"
#import "SoundMenuItem.h"
#import "SimpleAudioEngine.h"
#import "SelectCharScene.h"
@implementation HUD

+(id) HUDWithGameNode:(GameNode*)game
{
	return [[[self alloc] initWithGameNode:game] autorelease];
}
-(id) initWithGameNode:(GameNode*)aGame
{
	if( (self=[super init])) {
		
		self.isTouchEnabled = YES;
		game_ = aGame;

		CGSize s = [[CCDirector sharedDirector] winSize];
		
		[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"buttons.plist"];

		// level control configuration:
		//  - 2-way, 4-way or car ?
		//  - d-pad or accelerometer ?
		//  - 0, 1 or 2 buttons ?
	
		GameConfiguration *config = [GameConfiguration sharedConfiguration];
		ControlType control = [config controlType];
		ControlButton button = [config controlButton];
		
		if( [config controlDirection] == kControlDirection4WayCar )
			joystick_ = [JoystickCar joystick];
		else
			joystick_ = [Joystick joystick];
		[self addChild:joystick_];

		switch (button) {
			case kControlButton0:
				[joystick_ setButton:BUTTON_A enabled:NO];
			case kControlButton1:
				[joystick_ setButton:BUTTON_B enabled:NO];
				break;
			case kControlButton2:
				// both buttons are enabled by default, no need to modify it
				break;
		}
		
		// The Hero is responsible for reading the joystick
		[[game_ hero] setJoystick:joystick_];		
		
		// enable button left/right only if using "Pad" controls
		
		[joystick_ setPadEnabled: NO];
		// pad + 4 direction is not implemented yet
		if( control==kControlTypePad) {

			[joystick_ setPadEnabled: YES];
			[joystick_ setPadPosition:ccp(74,74)];
		}
		
		        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            topMenuColorBar = [CCLayerColor layerWithColor:ccc4(32,32,32,128) width:s.width height:80];
            [topMenuColorBar setPosition:ccp(0,s.height-80)];
            [self addChild:topMenuColorBar z:0];
            
            // Score Label
            CCLabelBMFont *scoreLabel = [CCLabelBMFont labelWithString:@"Score:" fntFile:@"comicw18-hd.fnt"];
            [scoreLabel.texture setAliasTexParameters];
            [topMenuColorBar addChild:scoreLabel z:1];
            [scoreLabel setPosition:ccp(s.width/2-100, 20.5f)];
            score_ = [CCLabelBMFont labelWithString:@"000" fntFile:@"comicy18-hd.fnt"];
            // Lives label
            CCLabelBMFont *livesLabel = [CCLabelBMFont labelWithString:@"Lives:" fntFile:@"comicw18-hd.fnt"];
            [livesLabel.texture setAliasTexParameters];
            [topMenuColorBar addChild:livesLabel z:1];
            [livesLabel setAnchorPoint:ccp(1,0.5f)];
            [livesLabel setPosition:ccp(s.width-100, 20.5f)];		
            lives_ = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%d", game_.lives] fntFile:@"comicy18-hd.fnt"];
            
        }else{
            topMenuColorBar = [CCLayerColor layerWithColor:ccc4(32,32,32,128) width:s.width height:40];
            [topMenuColorBar setPosition:ccp(0,s.height-40)];
            [self addChild:topMenuColorBar z:0];
            
            // Score Label
            CCLabelBMFont *scoreLabel = [CCLabelBMFont labelWithString:@"Score:" fntFile:@"comicw18.fnt"];
            [scoreLabel.texture setAliasTexParameters];
            [topMenuColorBar addChild:scoreLabel z:1];
            [scoreLabel setPosition:ccp(s.width/2-50, 20.5f)];
            score_ = [CCLabelBMFont labelWithString:@"000" fntFile:@"comicy18.fnt"];
            // Lives label
            CCLabelBMFont *livesLabel = [CCLabelBMFont labelWithString:@"Lives:" fntFile:@"comicw18.fnt"];
            [livesLabel.texture setAliasTexParameters];
            [topMenuColorBar addChild:livesLabel z:1];
            [livesLabel setAnchorPoint:ccp(1,0.5f)];
            [livesLabel setPosition:ccp(s.width-50, 20.5f)];		
            lives_ = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%d", game_.lives] fntFile:@"comicy18.fnt"];
        }
        // Menu Button
		CCMenuItem *itemBack = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(buttonRestart:)];
		CCMenuItem *itemPause = [SoundMenuItem itemFromNormalSpriteFrameName:@"pause_up.png" selectedSpriteFrameName:@"pause_down.png" target:self selector:@selector(buttonPause:)];
        topMenu = [CCMenu menuWithItems:itemBack,itemPause,nil];
        [topMenu alignItemsHorizontallyWithPadding:15.0f];
		[self addChild:topMenu z:1];
		[topMenu setPosition:ccp(50,s.height-20)];
		
		// Score Points
		[score_.texture setAliasTexParameters];
		[topMenuColorBar addChild:score_ z:1];
		[score_ setPosition:ccp(s.width/2+0.5f+35, 20.5f)];
		
		[lives_.texture setAliasTexParameters];
		[topMenuColorBar addChild:lives_ z:1];
		//[lives_ setAnchorPoint:ccp(1,0.5f)];
		[lives_ setPosition:ccp(s.width-25.5f, 20.5f)];	
        
        [self onUpdateScore:[[GlobalSingleton sharedInstance] gamePoints]];		
	}
	
	return self;
}

-(void)displayKey:(int)key{
    switch (key) {
        case 1:
            key1 = [CCSprite spriteWithSpriteFrameName:@"key.png"];
            [self addChild:key1];
            [key1 setPosition:ccp(440,40)];
            break;
        case 2:
            key2 = [CCSprite spriteWithSpriteFrameName:@"key.png"];
            [self addChild:key2];
            [key2 setPosition:ccp(440,60)];
            break;
        case 3:
            key3 = [CCSprite spriteWithSpriteFrameName:@"key.png"];
            [self addChild:key3];
            [key3 setPosition:ccp(440,80)];
            break;
            
        default:
            break;
    }
}

-(void)removeKey:(int)key{
    switch (key) {
        case 1:
            [self removeChild:key1 cleanup:YES];
            break;
        case 2:
            [self removeChild:key2 cleanup:YES];
            break;
        case 3:
            [self removeChild:key3 cleanup:YES];
            break;
        default:
            break;
    }
}



-(void) backCallback:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[SelectLevelScene scene] withColor:ccWHITE]];
}

-(void) onUpdateScore:(int)newScore
{
    [[GlobalSingleton sharedInstance] setGamePoints:newScore];
	[score_ setString: [NSString stringWithFormat:@"%03d", newScore]];
	[score_ stopAllActions];
	id scaleTo = [CCScaleTo actionWithDuration:0.1f scale:1.2f];
	id scaleBack = [CCScaleTo actionWithDuration:0.1f scale:1];
	id seq = [CCSequence actions:scaleTo, scaleBack, nil];
	[score_ runAction:seq];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if([[GlobalSingleton sharedInstance] bannerType]==0){
            //none
            CGSize s = [[CCDirector sharedDirector] winSize];
            [topMenu setPosition:ccp(100,s.height-40)];
            [topMenuColorBar setPosition:ccp(0,s.height-80)];
        }else if([[GlobalSingleton sharedInstance] bannerType]==1){
            //admob
            CGSize s = [[CCDirector sharedDirector] winSize];
            [topMenu setPosition:ccp(100,s.height-130)];
            [topMenuColorBar setPosition:ccp(0,s.height-170)];
        }
    }else{
        if([[GlobalSingleton sharedInstance] bannerType]==0){
            //none
            CGSize s = [[CCDirector sharedDirector] winSize];
            [topMenu setPosition:ccp(50,s.height-20)];
            [topMenuColorBar setPosition:ccp(0,s.height-40)];	
        }else if([[GlobalSingleton sharedInstance] bannerType]==1){
            //admob
            CGSize s = [[CCDirector sharedDirector] winSize];
            [topMenu setPosition:ccp(50,s.height-65)];
            [topMenuColorBar setPosition:ccp(0,s.height-85)];
        }
    }
}
-(void) onUpdateLives:(int)newLives
{
	[lives_ setString: [NSString stringWithFormat:@"%d", newLives]];
	[lives_ runAction:[CCBlink actionWithDuration:0.5f blinks:5]];
}
-(void)resetMessageOnScreen{
    if(messageOnScreen)NSLog(@"REMOVE");
    if(!messageOnScreen)NSLog(@"NOT REMOVE");
    messageOnScreen = NO;
    
}
-(void)displayTextMessage:(NSString*)message
{
    if(!messageOnScreen){
        messageOnScreen = YES;
        CGSize s = [[CCDirector sharedDirector] winSize];
        CCSprite* popOps = [CCSprite spriteWithSpriteFrameName:@"ops_key.png"];
        [self addChild:popOps];
        [popOps setPosition:ccp(s.width/2, s.height/2 + 30)];
        
        
      //  CCLabelTTF *label = [CCLabelTTF labelWithString:message fontName:@"Marker Felt" fontSize:54];
     //   label.scale = 0.01;
     //   [self addChild:label];
       // [label setPosition:ccp(s.width/2, s.height/2 + 30)];
    
        id rot1 = [CCScaleTo actionWithDuration:0.05f scale:1.2f]; 
        id rot2 = [CCScaleTo actionWithDuration:0.05f scale:1.0f]; 
        id rot3 = [CCDelayTime actionWithDuration:1];
        id rot4 = [CCScaleTo actionWithDuration:0.05f scale:1.2f]; 
        id rot5 = [CCScaleTo actionWithDuration:0.05f scale:0.0f]; 
        id removeMessageAction = [CCCallFunc actionWithTarget:self selector:@selector(resetMessageOnScreen)];
        id removeSpriteAction = [CCCallFuncND actionWithTarget:popOps selector:@selector(removeFromParentAndCleanup:) data:(void*)YES];
      //  id seq = [CCSequence actions:rot1, rot2, rot3, rot4,rot5,removeMessageAction ,removeSpriteAction, nil];
        id seq = [CCSequence actions:rot1, rot2, rot3, rot4,rot5, rot3,removeMessageAction, removeSpriteAction , nil];
        [popOps runAction:seq];
    }
	
}
-(void) displayMessage:(NSString*)message
{
    /* multiline
    Label *lbl01 = [Label labelWithString:NSLocalizedString(@"tut_0101", @"") dimensions:CGSizeMake(300, 60) alignment:UITextAlignmentLeft fontName:@"Helvetica" fontSize:16];
    [lbl01 setPosition: CGPointMake(160,330)];
    [self addChild: lbl01];
    */
	CGSize s = [[CCDirector sharedDirector] winSize];
	CCSprite* popEnd = [CCSprite spriteWithSpriteFrameName:@"end_level.png"];
    [self addChild:popEnd];
    [popEnd setPosition:ccp(s.width/2, s.height/2 - 30)];

    
	CCLabelTTF *label = [CCLabelTTF labelWithString:message fontName:@"Marker Felt" fontSize:14];
	[self addChild:label];
	[label setPosition:ccp(s.width/2, s.height/2 + 10)];

	id sleep = [CCDelayTime actionWithDuration:3];
	id rot1 = [CCRotateBy actionWithDuration:0.025f angle:5];
	id rot2 = [rot1 reverse];
	id rot3 = [CCRotateBy actionWithDuration:0.05f angle:-5];
	id rot4 = [rot3 reverse];
	id seq = [CCSequence actions:rot1, rot2, rot3, rot4, nil];
	id repeat_rot = [CCRepeat actionWithAction:seq times:3];
	id big_seq = [CCSequence actions:sleep, repeat_rot, nil];
	id repeat_4ever = [CCRepeatForever actionWithAction:big_seq];
	[label runAction:repeat_4ever];
	[popEnd runAction:repeat_4ever];
	CCMenuItem *item1 = [CCMenuItemFont itemWithString:@"Select Player" target:self selector:@selector(selectPlayer:)];
	CCMenuItem *item2 = [CCMenuItemFont itemWithString:@"Select Level" target:self selector:@selector(selectLevel:)];
	CCMenu *menu = [CCMenu menuWithItems:item1, item2, nil];
	[menu alignItemsVertically];
	[menu setPosition:ccp(s.width/2, s.height/2.8)];
	[self addChild:menu z:10];
}

-(void) selectPlayer:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[SelectCharScene scene] ] ];
}
-(void) selectLevel:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[SelectLevelScene scene] ] ];
}

-(void) playAgain:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:0.5f scene:[[game_ class] scene] ] ];
}

-(void) mainMenu:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
}

-(void) buttonRestart:(id)sender
{
//    [game_ togglePause];
	[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
}

-(void) buttonPause:(id)sender
{
    
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton unlockItem:@"gameIsPaused"];
	//[[CCDirector sharedDirector] replaceScene: [CCTransitionCrossFade transitionWithDuration:1 scene:[MenuScene scene]]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Paused" message:@"Press the button to..." delegate:self cancelButtonTitle:@"Resume" otherButtonTitles:nil];
    [alert show];
    
    [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    [CDAudioManager sharedManager].mute = TRUE;
    [[CCDirector sharedDirector] pause];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton lockItem:@"gameIsPaused"];
	
    [[CCDirector sharedDirector] resume];
    [CDAudioManager sharedManager].mute = FALSE;
    [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
}

- (void) dealloc
{
	[super dealloc];
}


#pragma mark Touch Handling

-(void) registerWithTouchDispatcher
{
	// Priorities: lower number, higher priority
	// Joystick: 10
	// GameNode (dragging objects): 50
	// HUD (dragging screen): 100
	[[[CCDirector sharedDirector] touchDispatcher] addTargetedDelegate:self priority:100 swallowsTouches:YES];
}

-(BOOL) ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event
{
	return YES;
}

-(void) ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event
{
}

-(void) ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event
{
}

// drag the screen
-(void) ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event
{
    /*
	CGPoint touchLocation = [touch locationInView: [touch view]];	
	CGPoint prevLocation = [touch previousLocationInView: [touch view]];	
	
	touchLocation = [[CCDirector sharedDirector] convertToGL: touchLocation];
	prevLocation = [[CCDirector sharedDirector] convertToGL: prevLocation];
	
	CGPoint diff = ccpSub(touchLocation,prevLocation);
	game_.cameraOffset = ccpAdd( game_.cameraOffset, diff );
    */
}

@end
