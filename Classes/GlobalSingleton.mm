//
//  GlobalSingleton.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//
#import "cocos2d.h"
#import "GlobalSingleton.h"
@implementation GlobalSingleton

static GlobalSingleton *_sharedInstance;

@synthesize bannerType,audioOn,gameType,gameKitState,deviceType, keyLocker,princess,playerChar,princessName,gamePoints;

- (id) init {
	self = [super init];
	if (self != nil){
		audioOn = YES;
		
		// custom initialization
		memset(board, 0, sizeof(board));
	}
	return self;
}

+ (GlobalSingleton *) sharedInstance
{
	if (!_sharedInstance)
	{
		_sharedInstance = [[GlobalSingleton alloc] init];
        [_sharedInstance setBannerType:0];
        [_sharedInstance setPlayerChar:@"skull"];
        [_sharedInstance unlockItem:@"skull"];
        
	}
	
	return _sharedInstance;
}

-(int)addKey{
       return keyLocker = keyLocker+1;
    
}
-(int)removeKey{
     return keyLocker = keyLocker-1;
}

-(void)setGamePoints:(int)points{
    gamePoints = points;
    [self intPref:points forKey:@"gamePoints"];

}
-(int)gamePoints{
    return [self getIntPrefForKey:@"gamePoints"];
}


-(void)unlockItem:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:item];
}

-(void)lockItem:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:item];
}

-(BOOL)isItemUnlocked:(NSString*)item{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs boolForKey:item]){
        return YES;
    }else{
        return NO;
    }
}


-(void)intPref:(int)integ forKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:integ forKey:key];
}

//if no pref return -1, else return the preference
-(int)getIntPrefForKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   return [prefs integerForKey:key];
}

-(NSString*)timeToString:(int)t{
    int minutes = t / 60000;
    int seconds = t % 60000 / 1000;
    int milliseconds = t % 1000;
    
    NSString* minutesString;
    NSString* secondsString;
    NSString* millisecondsString;
    
   // if(minutes<=9){
   //     minutesString = [NSString stringWithFormat:@"0%d",minutes];
   // }else{
        minutesString = [NSString stringWithFormat:@"%d",minutes];
    //}
    if(seconds<=9){
        secondsString = [NSString stringWithFormat:@"0%d",seconds];
    }else{
        secondsString = [NSString stringWithFormat:@"%d",seconds];
    }
    if(milliseconds<=9){
        millisecondsString = [NSString stringWithFormat:@"00%d",milliseconds];
    }else if(milliseconds<=99){
        millisecondsString = [NSString stringWithFormat:@"0%d",milliseconds];
    }else{
        millisecondsString = [NSString stringWithFormat:@"%d",milliseconds];
    }
    // NSLog(@"TIMER TIME - %@ : %@ : %@ ",minutesString, secondsString, millisecondsString);
    return [NSString stringWithFormat:@"%@ : %@ : %@ ",minutesString, secondsString, millisecondsString];
    
}


-(void) clearData {
	
	audioOn = YES;
    keyLocker = 0;
   
}



- (id)retain

{
	
    return self;
	
}



- (unsigned)retainCount

{
	
    return UINT_MAX;  //denotes an object that cannot be released
	
}



- (void)release

{
	
    //do nothing
	
}



- (id)autorelease

{
	
    return self;
	
}




@end