//
//  Door1.mm
//  LevelSVG
//
//  Created by Gil Beyruth on 12/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Door1.h"
#import "GlobalSingleton.h"

#define SPRITE_DOOR_WIDTH 32
#define SPRITE_DOOR_HEIGHT 64
//
// Fruit: a sensor with the shape of an apple
// When touched, it will increase points in 1
//

@implementation Door1

-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {

		CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"door1-locked.png"];
        [self setDisplayFrame:frame];

        reportContacts_ = BN_CONTACT_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
		isTouchable_ = YES;
		
        // 1. destroy already created fixtures
		[self destroyAllFixturesFromBody:body];
		
        // 2. create new fixture
		b2FixtureDef	fd;
		
		b2PolygonShape shape;
		
		// Sprite size is SPRITE_DOOR_WIDTH x SPRITE_DOOR_HEIGHT
		float height = (SPRITE_DOOR_HEIGHT / kPhysicsPTMRatio) / 2;
		float width = (SPRITE_DOOR_WIDTH/ kPhysicsPTMRatio) / 2;
		
		// vertices should be in Counter Clock-Wise order, orderwise it will crash
		b2Vec2 vertices[4];
		vertices[0].Set(-width,-height);	// bottom-left
		vertices[1].Set(width,-height);		// bottom-right
		vertices[2].Set(width,height);		// top-right
		vertices[3].Set(-width,height);		// top-left
		shape.Set(vertices, 4);
        
        
        fd.friction		= kPhysicsDefaultEnemyFriction;
		fd.density		= kPhysicsDefaultEnemyDensity;
		fd.restitution	= kPhysicsDefaultEnemyRestitution;
		fd.shape = &shape;
		
		// filtering... in case you want to avoid collisions between enemies
        //		fd.filter.groupIndex = - kCollisionFilterGroupIndexEnemy;
		
		body->CreateFixture(&fd);
		body->SetType(b2_staticBody);	
        
        
	}
	return self;
}

-(void) setParameters:(NSDictionary *)params
{
	[super setParameters:params];
	NSString *type = [params objectForKey:@"type"];
	NSString *key = [params objectForKey:@"key"];
	
	if( type )
		type_ = [type floatValue];	
    
    if( key )
		keyLocker_ = [key floatValue];	
	/*if(type_){
    CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"door%@-locked.png",type_]];
    [self setDisplayFrame:frame];
    }*/
}

-(void) touchedByHero
{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    NSLog(@"%i",[mySingleton keyLocker]);
    if([mySingleton keyLocker]>=1){
        [game_ displayGameImageMessage:@"unlocked.png" positionX:self.position.x positionY:self.position.y];
        [[SimpleAudioEngine sharedEngine] playEffect: @"coin1.wav"];
        [game_ removeKey];
        [game_ removeB2Body:body_];
        
    }else{
        [game_ displayGameTextMessage:@"You must get this door key to open it!"];
        [game_ pushHero:self];
        //colocar audio de cadeado
        [[SimpleAudioEngine sharedEngine] playEffect: @"coin1.wav"];
    }
}
@end
