//
//  AboutNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "SelectCharScene.h"
#import "MenuScene.h"
#import "SoundMenuItem.h"
#import "SoundMenuItemLabel.h"
#import "MKStoreKit.h"
#import "GlobalSingleton.h"
#import "SelectLevelScene.h"


@implementation SelectCharScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [SelectCharScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init]) )
	{
		
		CGSize s = [[CCDirector sharedDirector] winSize];
	
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"select_screen.plist" textureFile:@"select_screen.pvr.ccz"];
        
        
        CCSprite *background = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            background = [CCSprite spriteWithFile:@"select_player_screen-hd.gif"];
        }else{
            background = [CCSprite spriteWithFile:@"select_player_screen.gif"];
        }    
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
        CCMenu *menu = nil;
        
        
		SoundMenuItem * item0;
        SoundMenuItem * item1;
        SoundMenuItem * item2;
        SoundMenuItem * item3;
        SoundMenuItem * item4;
        SoundMenuItem * item5;
        SoundMenuItem * item6;
       
        SoundMenuItem * item7;
        SoundMenuItem * item8;
        SoundMenuItem * item9;
        SoundMenuItem * item10;
        SoundMenuItem * item11;
        
        item0 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_skull.png" selectedSpriteFrameName:@"bt_skull.png"  target:self selector:@selector(char1:)];
        
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level1"]){
            item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_mummy.png" selectedSpriteFrameName:@"bt_mummy.png" target:self selector:@selector(char2:)];
        }else{
            item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_mummy_locked.png" selectedSpriteFrameName:@"bt_mummy_locked.png" target:self selector:@selector(char2:)];	
        }
        
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level2"]){
            
            item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_ginger.png" selectedSpriteFrameName:@"bt_ginger.png" target:self selector:@selector(char3:)];
        }else{
            item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_ginger_locked.png" selectedSpriteFrameName:@"bt_ginger_locked.png" target:self selector:@selector(char3:)];
        }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level3"]){
            item3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_tourmaline.png" selectedSpriteFrameName:@"bt_tourmaline.png"  target:self selector:@selector(char4:)];
        }else{
            item3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_tourmaline_locked.png" selectedSpriteFrameName:@"bt_tourmaline_locked.png" target:self selector:@selector(char4:)];
        }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level4"]){
            item4 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_casper.png" selectedSpriteFrameName:@"bt_casper.png"  target:self selector:@selector(char5:)];
        }else{
            item4 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_casper_locked.png" selectedSpriteFrameName:@"bt_casper_locked.png" target:self selector:@selector(char5:)];
        }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level5"]){
            item5 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_scarlet.png" selectedSpriteFrameName:@"bt_scarlet.png"  target:self selector:@selector(char6:)];
        }else{
            item5 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_scarlet_locked.png" selectedSpriteFrameName:@"bt_scarlet_locked.png" target:self selector:@selector(char6:)];
        }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level6"]){
            item6 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_teenwolf.png" selectedSpriteFrameName:@"bt_teenwolf.png" target:self selector:@selector(char7:)];
        }else{
            item6 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_teenwolf_locked.png" selectedSpriteFrameName:@"bt_teenwolf_locked.png"  target:self selector:@selector(char7:)];
        }
        
        ///COMMENT FROM HERE ON
        
        
        
         if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level7"]){
            item7 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_ruby.png" selectedSpriteFrameName:@"bt_ruby.png" target:self selector:@selector(char8:)];
         }else{
            item7 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_ruby_locked.png" selectedSpriteFrameName:@"bt_ruby_locked.png" target:self selector:@selector(char8:)];
         }
         if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level8"]){
            item8 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_blonde.png" selectedSpriteFrameName:@"bt_blonde.png"   target:self selector:@selector(char9:)];
         }else{
            item8 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_blonde_locked.png" selectedSpriteFrameName:@"bt_blonde_locked.png" target:self selector:@selector(char9:)];
         }
         if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level9"]){
            item9 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_beth.png" selectedSpriteFrameName:@"bt_beth.png"  target:self selector:@selector(char10:)];
         }else{
            item9 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_beth_locked.png" selectedSpriteFrameName:@"bt_beth_locked.png"  target:self selector:@selector(char10:)];
         }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level10"]){
            item10 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_pumpkin.png" selectedSpriteFrameName:@"bt_pumpkin.png"  target:self selector:@selector(char11:)];
        }else{
            item10 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_pumpkin_locked.png" selectedSpriteFrameName:@"bt_pumpkin_locked.png"  target:self selector:@selector(char11:)];
        }
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level11"]){
            item11 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_draco.png" selectedSpriteFrameName:@"bt_draco.png"  target:self selector:@selector(char12:)];
        }else{
            item11 = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_draco_locked.png" selectedSpriteFrameName:@"bt_draco_locked.png"  target:self selector:@selector(char12:)];
        }
        
        
            
            
           // menu = [CCMenu menuWithItems: item0, item1, item2, item3, item4, item5,  item6,  nil]; 
            menu = [CCMenu menuWithItems: item0, item1, item2, item3, item4, item5,  item6,   item7, item8, item9, item10,item11, nil];
            
            [menu alignItemsInColumns:
             [NSNumber numberWithUnsignedInt:4],
             [NSNumber numberWithUnsignedInt:4],
              [NSNumber numberWithUnsignedInt:4],
             nil
             ];
       		
		
        
        //2 + 2 + 2 + 2 + 2 = total count of 10
		[self addChild:menu];
        [menu setPosition:ccp(s.width/2,s.height/2-30)];
		
        //back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
        //backButton.position = ccp(15,s.height+20);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-100)];
            }
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-50)];
            }
        }
        
        
		backButton.anchorPoint = ccp(0,1);
        
		menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
		
	}
	
	return self;

}
-(void) char1:(id)sender
{
    
        [[GlobalSingleton sharedInstance] setPlayerChar:@"skull"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
   	
}

-(void) char2:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level1"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"mummy"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"The Mummy" 
                                                        message:@"You must save Mummy first. When you have saved him, he will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
	
}
-(void) char3:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level2"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"yellowkid"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Ginger" 
                                                        message:@"You must save Mrs. Ginger first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}
-(void) char4:(id)sender
{
    
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level3"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"blue"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Tourmaline" 
                                                        message:@"You must save Mrs. Tourmaline first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}
-(void) char5:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level4"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"ghost"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Lil. Casper" 
                                                        message:@"You must save Lil. Casper first. When you have saved him, he will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}
-(void) char6:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level5"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"red"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Scarlet" 
                                                        message:@"You must save Mrs. Scarlet first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}


-(void) char7:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level6"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"wolf"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Teen Wolf"
                                                        message:@"You must save Teen Wolf first. When you have saved him, he will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}

-(void) char8:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level7"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"redkid"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ruby"
                                                        message:@"You must save Ruby first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}

-(void) char9:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level8"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"yellowkid"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Blonde"
                                                        message:@"You must save Blonde first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}

-(void) char10:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level9"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"bluekid"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Beth"
                                                        message:@"You must save Beth first. When you have saved her, she will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}

-(void) char11:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level10"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"pumpkin"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Pumpkin"
                                                        message:@"You must save Pumpkin first. When you have saved him, he will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}


-(void) char12:(id)sender
{
    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level11"]){
        [[GlobalSingleton sharedInstance] setPlayerChar:@"draco"];
        [[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectLevelScene scene] ]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Draco"
                                                        message:@"You must save Draco first. When you have saved him, he will help you save the others." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
        
    }
	
}




-(void) backCallback:(ccTime)dt
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:1.0f scene:[MenuScene scene]]];
}
@end
