//
//  ShoppingCartScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "ShoppingCartScene.h"
#import "MenuScene.h"
#import "GlobalSingleton.h"
#import "MKStoreKit.h"
#import "SoundMenuItem.h"
#import "RootViewController.h"
#import "AppDelegate.h"


@implementation ShoppingCartScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [ShoppingCartScene node];
	
	[scene addChild:child];
	return scene;
}


- (id) init {
    self = [super init];
    if (self != nil) {
        CGSize s = [[CCDirector sharedDirector] winSize];
		
        CCSprite * bg = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            bg = [CCSprite spriteWithFile:@"main-menu-background-hd.png"];
        }else{
            bg = [CCSprite spriteWithFile:@"main-menu-background.png"];
        }
        
        
        
        [bg  setPosition:ccp(s.width/2, s.height/2)];
        [self addChild:bg z:0];
      
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchasedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          NSLog(@"Purchased product with id: %@", [note object]);
                                                          NSLog(@"%@", [[MKStoreKit sharedKit] valueForKey:@"purchaseRecord"]);
                                                          [[GlobalSingleton sharedInstance] unlockItem:@"com.firstpixel.feargarden.banner"];
                                                          //TODO Add this to the response of the buyer
                                                          
                                                          RootViewController *rootController = (RootViewController *)[[(AppDelegate*)[[UIApplication sharedApplication]delegate] window] rootViewController];
                                                          
                                                          [(RootViewController *)rootController removeBanner];
                                                          //messageLoading = [CCLabelTTF labelWithString:@"Purchased package 1" fontName:@"Arial" fontSize:17];
                                                      }];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductsAvailableNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSLog(@"Products available: %@  %@", [[MKStoreKit sharedKit] availableProducts], [[MKStoreKit sharedKit] availableProducts][0]);
                                                      }];
        
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoredPurchasesNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                         // messageLoading = [CCLabelTTF labelWithString:@"Restored Purchases" fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Restored Purchases");
                                                          [[GlobalSingleton sharedInstance] unlockItem:@"com.firstpixel.feargarden.banner"];
                                                          //TODO Add this to the response of the buyer
                                                          RootViewController *rootController =(RootViewController *)[[(AppDelegate*)[[UIApplication sharedApplication]delegate] window] rootViewController];
                                                          [(RootViewController *)rootController removeBanner];
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoringPurchasesFailedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          
                                                          NSString* message = [NSString stringWithFormat:@"Failed restoring purchases with error: %@", [note object]];
                                                          //messageLoading = [CCLabelTTF labelWithString:message fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Failed restoring purchases with error: %@", [note object]);
                                                      }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchaseFailedNotification
                                                          object:nil
                                                           queue:[[NSOperationQueue alloc] init]
                                                      usingBlock:^(NSNotification *note) {
                                                          NSString* message = [NSString stringWithFormat:@"Failed purchases with error: %@", [note object]];
                                                          //messageLoading = [CCLabelTTF labelWithString:message fontName:@"Arial" fontSize:17];
                                                          NSLog(@"Failed purchases with error: %@", [note object]);
                                                      }];
        
        
        [[MKStoreKit sharedKit] startProductRequest];
        
        
        
        
        
        
        
		[[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.firstpixel.feargarden.full2"];

		
        
        //back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
        //backButton.position = ccp(15,s.height+20);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-100)];
            }
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-50)];
            }
        }
        
        
		backButton.anchorPoint = ccp(0,1);
        
		CCMenu* menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        
        CCLabelBMFont* messageLoading = [CCLabelBMFont labelWithString:@"LOADING STORE...\nIf it takes too long,\n check your internet\n connection." fntFile:@"fear2.fnt"];
        [messageLoading setPosition:ccp(s.width/2, s.height/2-40)];
        [self addChild:messageLoading z:2];
		
       
		
    }
    return self;
}

-(void)backCallback:(id)sender{
	[[CCDirector sharedDirector] replaceScene:[MenuScene scene]];
}




- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	//NSLog(@"-------------------------------------------------------------- Clicked began : %i , %i",location.x,location.y);
	location = CGPointMake(location.y, location.x);
	
	
	//return kEventHandled;	
}
@end
