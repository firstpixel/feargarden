//
//  EffectsNode.mm
//  LevelSVG
//
//  Created by Gil Beyruth on 10/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "EffectsNode.h"
#import "SimpleAudioEngine.h"
#import "GameNode.h"

//
// BadGuy: Base class of all enemies. 
// If the hero touches a EffectsNode, then the "touchedbyHero" method will be called.
//

@implementation EffectsNode
-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game] ) ) {
		
        properties_ = BN_PROPERTY_NONE;
        
		reportContacts_ = BN_CONTACT_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_EFFECTS;
		isTouchable_ = NO;
        isPlaying = NO;
		
	}
	return self;
}
-(void) touchedByHero
{
    if(isPlaying == NO){
        isPlaying = YES;
        [[SimpleAudioEngine sharedEngine] playEffect:@"grass.wav"];
	}
}

-(void) endContact:(b2Contact*) contact
{
    isPlaying = NO;
    
	// override me
}

@end
