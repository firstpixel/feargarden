//
//  AppDelegate.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 12/09/09.
//  Copyright Sapus Media 2009. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


// cocos2d and cocosdenshion imports
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

// local imports
#import "RootViewController.h"
#import "MKStoreKit.h"
#import "AppDelegate.h"
#import "MenuScene.h"


@interface AppDelegate (Sounds)
- (void) initSounds;
- (void) removeStartupFlicker;
@end

@implementation AppDelegate

@synthesize window=window_;
@synthesize navController=navController_;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if your Application only supports landscape mode
	//
	
	//	CC_ENABLE_DEFAULT_GL_STATES();
	//	CCDirector *director = [CCDirector sharedDirector];
	//	CGSize size = [director winSize];
	//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
	//	sprite.position = ccp(size.width/2, size.height/2);
	//	sprite.rotation = -90;
	//	[sprite visit];
	//	[[director openGLView] swapBuffers];
	//	CC_ENABLE_DEFAULT_GL_STATES();
}

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
    
    [[MKStoreKit sharedKit] startProductRequest];
    
    // Main Window
    window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Director
    director_ = (CCDirectorIOS*)[CCDirector sharedDirector];
    [director_ setDisplayStats:NO];
    [director_ setAnimationInterval:1.0/60];
    
    // GL View
    CCGLView *__glView = [CCGLView viewWithFrame:[window_ bounds]
                                     pixelFormat:kEAGLColorFormatRGB565
                                     depthFormat:0 /* GL_DEPTH_COMPONENT24_OES */
                              preserveBackbuffer:NO
                                      sharegroup:nil
                                   multiSampling:NO
                                 numberOfSamples:0
                          ];
    
    __glView.multipleTouchEnabled = YES;
    [director_ setView:__glView];
    [director_ setDelegate:self];
    director_.wantsFullScreenLayout = YES;
    
    // 2D projection
    [director_ setProjection:kCCDirectorProjection2D];
    //	[director setProjection:kCCDirectorProjection3D];
    
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    if( ! [director_ enableRetinaDisplay:YES] )
        CCLOG(@"Retina Display Not supported");
    
    // Default texture format for PNG/BMP/TIFF/JPEG/GIF images
    // It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
    // You can change anytime.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
    // Assume that PVR images have premultiplied alpha
    [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
    CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
    [sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
    [sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
    [sharedFileUtils setiPadSuffix:@"-hd"];					// Default on iPad is "ipad"
    [sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
    
    
    
    // Navigation Controller
    navController_ = [[RootViewController alloc] initWithRootViewController:director_];
    navController_.navigationBarHidden = YES;
    
    // AddSubView doesn't work on iOS6
    //	[window_ addSubview:navController_.view];
    [window_ setRootViewController:navController_];
    
    [window_ makeKeyAndVisible];
    
    
    
    // Init sounds
	[self initSounds];
	
	// Run
	[[CCDirector sharedDirector] runWithScene:[MenuScene scene]];
}

- (void)initSounds
{
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"pickup_coin.wav"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"you_won.wav"];
	[[SimpleAudioEngine sharedEngine] preloadEffect:@"you_are_hit.wav"];

}

- (void)applicationWillResignActive:(UIApplication *)application {
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
	[[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	// TIP:
	// Stop the director mainloop.
	// Callback only called in iOS >=4 and in multitasking devices
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	// TIP:
	// Resume the director mainloop.
	// Callback only called in iOS >=4 and in multitasking devices
	[[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{	
	// TIP:
	// Save the game state here
	CCDirector *director = [CCDirector sharedDirector];
	
	[[director view] removeFromSuperview];
	
	[navController_ release];
	
	[window_ release];
	
	[director end];	
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	[[CCDirector sharedDirector] end];
	[window_ release];
	[navController_ release];
	[super dealloc];
}

@end
