//
//  AboutNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "AboutScene.h"
#import "MenuScene.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "GameConstants.h"


@implementation AboutScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [AboutScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		CCSprite *background = nil;

        background = [CCSprite spriteWithFile:@"about-background.png"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites.plist"];
        spritesBatchNode_ = [CCSpriteBatchNode batchNodeWithFile:@"sprites.png" capacity:20];
         
		CGSize size = [[CCDirector sharedDirector] winSize];
		
        background.position = ccp(size.width/2, size.height/2);
		[self addChild:background];

        
        SoundMenuItem * item0;
        SoundMenuItem * item1;
        SoundMenuItem * item2;
              
        item0 = [SoundMenuItem itemFromNormalSpriteFrameName:@"link-firstpixel.png" selectedSpriteFrameName:@"link-firstpixel.png"  target:self selector:@selector(firstpixel)];
        
        item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"link-facebook.png" selectedSpriteFrameName:@"link-facebook.png" target:self selector:@selector(facebook)];
        
        item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"link-twitter.png" selectedSpriteFrameName:@"link-twitter.png" target:self selector:@selector(twitter)];
       
        
       CCMenu* menu = [CCMenu menuWithItems: item0, item1, item2, nil];
        
        [menu alignItemsHorizontally];        
		
        
        //2 + 2 + 2 + 2 + 2 = total count of 10
		[self addChild:menu];
        
        [menu setPosition:ccp(size.width/2,size.height/2-50)];
		//back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
        //backButton.position = ccp(15,s.height+20);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [menu setPosition:ccp(size.width/2,size.height/2-60)];
            
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-100)];
            }
            
            
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-50)];
            }
        }
        
        
		backButton.anchorPoint = ccp(0,1);
        
		menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
	}
	return self;
}

-(void) firstpixel{
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://firstpixel.com/site/games/"]];
}
-(void) twitter {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/#/firstpixel"]];
}
-(void) facebook {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Firstpixel/134776246539448"]];
}

-(void) backCallback:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[MenuScene scene] ]];
}
@end