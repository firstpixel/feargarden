//
//  EffectsNode.h
//  LevelSVG
//
//  Created by Gil Beyruth on 10/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "BodyNode.h"


@interface EffectsNode : BodyNode {
    BOOL isPlaying;
}

-(void) touchedByHero;
@end
