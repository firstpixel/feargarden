//
//  Door1.h
//  LevelSVG
//
//  Created by Gil Beyruth on 12/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "BodyNode.h"


@interface Door1 : BodyNode {
	float	type_;
    float   keyLocker_;
}
-(void) touchedByHero;
@end
