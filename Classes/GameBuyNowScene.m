//
//  GameBuyNowScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "GameBuyNowScene.h"
#import "ShoppingCartScene.h"

#import "SoundMenuItem.h"

@implementation GameBuyNowScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [GameBuyNowScene node];
	[s addChild:node];
	return s;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        CGSize s = [[CCDirector sharedDirector] winSize];
        
        SoundMenuItem *selectButton = nil;
        selectButton = [SoundMenuItem itemWithNormalSprite:[CCSprite spriteWithFile:@"buy_fullversion_screen.gif"] selectedSprite:[CCSprite spriteWithFile:@"buy_fullversion_screen.gif"] target:self selector:@selector(startShopping:)];
        
        
        CCMenu *menu = [CCMenu menuWithItems:selectButton, nil];
		menu.position = ccp(s.width/2,s.height/2);
		[self addChild: menu z:0];
    }
    return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

-(void)startShopping:(id)sender
{
     [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[ShoppingCartScene scene]]];
}

@end
