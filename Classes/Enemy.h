//
//  Enemy.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "BadGuy.h"


@interface Enemy : BadGuy <BodyNodeBulletProtocol> {

	BOOL	patrolActivated_;
	BOOL	patrolDirectionLeft_;
	BOOL	patrolSpiderActivated_;
	BOOL	patrolDirectionUp_;
	BOOL    patrolFlyActivated_;
    float	patrolTime_;
	float	patrolSpeed_;
    
	ccTime	patrolDT_;
    CCSpriteFrame *frame;
    int frameNumber;
    NSString *enemyTypeString;
}
-(void) touchedByHero;
@end
