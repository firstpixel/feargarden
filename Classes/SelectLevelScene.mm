//
//  SelectLevelScene.mm
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "SelectLevelScene.h"
#import "GameBuyNowScene.h"
#import "GlobalSingleton.h"
#import "Level1.h"
#import "Level2.h"
#import "Level3.h"
#import "Level4.h"
#import "Level5.h"
#import "Level6.h"
#import "Level7.h"
#import "Level8.h"
#import "Level9.h"
#import "Level10.h"
#import "Level11.h"

#import "MenuScene.h"
#import "SoundMenuItem.h"
#import "MKStoreKit.h"
#import "SelectCharScene.h"

@implementation SelectLevelScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [SelectLevelScene node];
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
		CGSize s = [[CCDirector sharedDirector] winSize];
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"select_screen.plist" textureFile:@"select_screen.pvr.ccz"];
        
		CCSprite *background = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            background = [CCSprite spriteWithFile:@"select-level-hd.png"];
        }else{
            background = [CCSprite spriteWithFile:@"select-level.png"];
        }    
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
        SoundMenuItem * item0;
        SoundMenuItem * item1;
        SoundMenuItem * item2;
        SoundMenuItem * item3;
        SoundMenuItem * item4;
        SoundMenuItem * item5;
        // SoundMenuItem * item6;
        // SoundMenuItem * item7;
        // SoundMenuItem * item8;
        // SoundMenuItem * item9;
        // SoundMenuItem * item10;
		item0 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_mummy.png" selectedSpriteFrameName:@"castle_mummy.png"  target:self selector:@selector(level1:)];
        if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level1"]){
            item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_ginger.png" selectedSpriteFrameName:@"castle_ginger.png" target:self selector:@selector(level2:)];
        }else{
            item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_ginger_locked.png" selectedSpriteFrameName:@"castle_ginger_locked.png" target:self selector:@selector(level2:)];	
        }
         CCMenu *menu = nil;
        if([[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.feargarden.full2"] == YES){
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level2"]){
                item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_tourmaline.png" selectedSpriteFrameName:@"castle_tourmaline.png" target:self selector:@selector(level3:)];
            }else{
                item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_tourmaline_locked.png" selectedSpriteFrameName:@"castle_tourmaline_locked.png" target:self selector:@selector(level3:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level3"]){
                item3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_casper.png" selectedSpriteFrameName:@"castle_casper.png" target:self selector:@selector(level4:)];
            }else{
                item3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_casper_locked.png" selectedSpriteFrameName:@"castle_casper_locked.png" target:self selector:@selector(level4:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level4"]){
                item4 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_scarlet.png" selectedSpriteFrameName:@"castle_scarlet.png" target:self selector:@selector(level5:)];
            }else{
                item4 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_scarlet_locked.png" selectedSpriteFrameName:@"castle_scarlet_locked.png" target:self selector:@selector(level5:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level5"]){
                item5 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_teenwolf.png" selectedSpriteFrameName:@"castle_teenwolf.png" target:self selector:@selector(level6:)];
            }else{
                item5 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_teenwolf_locked.png" selectedSpriteFrameName:@"castle_teenwolf_locked.png" target:self selector:@selector(level6:)];	
            }
            /*
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level6"]){
                item6 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_ruby.png" selectedSpriteFrameName:@"castle_ruby.png" target:self selector:@selector(level7:)];
            }else{
                item6 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_ruby_locked.png" selectedSpriteFrameName:@"castle_ruby_locked.png" target:self selector:@selector(level7:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level7"]){
                item7 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_blonde.png" selectedSpriteFrameName:@"castle_blonde.png" target:self selector:@selector(level8:)];
            }else{
                item7 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_blonde_locked.png" selectedSpriteFrameName:@"castle_blonde_locked.png" target:self selector:@selector(level8:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level8"]){
                item8 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_beth.png" selectedSpriteFrameName:@"castle_beth.png" target:self selector:@selector(level9:)];
            }else{
                item8 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_beth_locked.png" selectedSpriteFrameName:@"castle_beth_locked.png" target:self selector:@selector(level9:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level9"]){
                item9 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_pumpkin.png" selectedSpriteFrameName:@"castle_pumpkin.png" target:self selector:@selector(level10:)];
            }else{
                item9 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_pumpkin_locked.png" selectedSpriteFrameName:@"castle_pumpkin_locked.png" target:self selector:@selector(level10:)];	
            }
            if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level10"]){
                item10 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_draco.png" selectedSpriteFrameName:@"castle_draco.png" target:self selector:@selector(level11:)];
            }else{
                item10 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_draco_locked.png" selectedSpriteFrameName:@"castle_draco_locked.png" target:self selector:@selector(level11:)];	
            }
          */  
            menu = [CCMenu menuWithItems: item0, item1, item2, item3, item4, item5,  nil]; 
                                                                                        //], item6, item7, item8, item9,  item10, nil];
            [menu alignItemsInColumns:
             [NSNumber numberWithUnsignedInt:3],
             [NSNumber numberWithUnsignedInt:3],
             //[NSNumber numberWithUnsignedInt:2],
            // [NSNumber numberWithUnsignedInt:1],
            // [NSNumber numberWithUnsignedInt:2],
             nil
             ];
        }else{
            item2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_tourmaline_locked.png" selectedSpriteFrameName:@"castle_tourmaline_locked.png" target:self selector:@selector(buyNow:)];
            item3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_casper_locked.png" selectedSpriteFrameName:@"castle_casper_locked.png" target:self selector:@selector(buyNow:)];
            item4 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_scarlet_locked.png" selectedSpriteFrameName:@"castle_scarlet_locked.png" target:self selector:@selector(buyNow:)];	
            item5 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_teenwolf_locked.png" selectedSpriteFrameName:@"castle_teenwolf_locked.png" target:self selector:@selector(buyNow:)];
         /*  item6 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_ruby_locked.png" selectedSpriteFrameName:@"castle_ruby_locked.png" target:self selector:@selector(buyNow:)]; 
            item7 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_blonde_locked.png" selectedSpriteFrameName:@"castle_blonde_locked.png" target:self selector:@selector(buyNow:)];
            item8 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_beth_locked.png" selectedSpriteFrameName:@"castle_beth_locked.png" target:self selector:@selector(buyNow:)];
            item9 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_pumpkin_locked.png" selectedSpriteFrameName:@"castle_pumpkin_locked.png" target:self selector:@selector(buyNow:)];
            item10 = [SoundMenuItem itemFromNormalSpriteFrameName:@"castle_draco_locked.png" selectedSpriteFrameName:@"castle_draco_locked.png" target:self selector:@selector(buyNow:)];	
          */
           // menu = [CCMenu menuWithItems: item0, item1, item2, item3, item4, item5,item6, item7, item8, item9, item10, nil];
            menu = [CCMenu menuWithItems: item0, item1, item2, item3, item4, item5, nil];
            [menu alignItemsInColumns:
             [NSNumber numberWithUnsignedInt:2],
             [NSNumber numberWithUnsignedInt:2],
             [NSNumber numberWithUnsignedInt:2],
           //  [NSNumber numberWithUnsignedInt:1],
            // [NSNumber numberWithUnsignedInt:2],
             nil
             ];
            
        }
        [menu setPosition:ccp(s.width/2,s.height/2-30)];
        //2 + 2 + 2 + 2 + 2 = total count of 10
		[self addChild:menu];
		//back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
            //backButton.position = ccp(15,s.height+20);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-100)];
            }
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-50)];
            }
        }
		backButton.anchorPoint = ccp(0,1);
		menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
	}
	return self;
}

-(void) setLevelScene:(Class)klass
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionSplitRows transitionWithDuration:1 scene:[klass scene]]];
}

-(void) buyNow:(id)sender
{
	[self setLevelScene:[GameBuyNowScene class]];
}
-(void) level1:(id)sender
{
    [[GlobalSingleton sharedInstance] setPrincess:@"mummy"];
    
    [[GlobalSingleton sharedInstance] setPrincessName:@"The Mummy"];
	[self setLevelScene:[Level1 class]];
}
-(void) level2:(id)sender
{
        
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level1"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"yellow"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Mrs. Ginger"];

        [self setLevelScene:[Level2 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Ginger" 
                                                        message:@"You must save The Mummy first. When you have saved him, he will give you the key for Mrs. Ginger's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void) level3:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level2"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"blue"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Mrs. Tourmaline"];
        [self setLevelScene:[Level3 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Tourmaline" 
                                                        message:@"You must save Mrs. Ginger first. When you have saved him, he will give you the key for Tourmaline's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void) level4:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level3"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"ghost"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Lil. Casper"];
        [self setLevelScene:[Level4 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Lil. Casper" 
                                                        message:@"You must save Mrs Tourmaline first. When you have saved her, she will give you the key for Haunted castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level5:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level4"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"red"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Mrs. Scarlet"];
        [self setLevelScene:[Level5 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mrs. Scarlet" 
                                                        message:@"You must save Lil Gasper first. When you have saved him, he will give you the key for Scarlet's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level6:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level5"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"wolf"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Teen Wolf"];
        [self setLevelScene:[Level6 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Teen Wolf" 
                                                        message:@"You must save Mrs. Scarlet first. When you have saved her, she will give you the key for Wolf's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level7:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level6"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"redkid"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Ruby"];
        [self setLevelScene:[Level7 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ruby" 
                                                        message:@"You must save Teen Wolf first. When you have saved him, he will give you the key for Ruby's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level8:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level7"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"yellowkid"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Blonde"];
        [self setLevelScene:[Level8 class]];
        
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Blonde" 
                                                        message:@"You must save Ruby first. When you have saved her, she will give you the key for Blonde's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level9:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level8"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"bluekid"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Beth"];
        [self setLevelScene:[Level9 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Beth" 
                                                        message:@"You must save Blonde first. When you have saved her, she will give you the key for Beth's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

-(void) level10:(id)sender
{    
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level9"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"pumpkin"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Mr. Pumpkin"];
        [self setLevelScene:[Level10 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Mr. Pumpkin" 
                                                        message:@"You must save Beth first. When you have saved her, she will give you the key for Mr. Pumpikin's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void) level11:(id)sender
{
    if([[GlobalSingleton sharedInstance] isItemUnlocked:@"level10"]){
        [[GlobalSingleton sharedInstance] setPrincess:@"draco"];
        [[GlobalSingleton sharedInstance] setPrincessName:@"Don Draco"];
        [self setLevelScene:[Level11 class]];
    }else{
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Don Draco" 
                                                        message:@"You must save Mr. Pumpikin first. When you have saved him, he will give you the key for Don Draco's castle." 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void) backCallback:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[SelectCharScene scene] ]];
}
@end
