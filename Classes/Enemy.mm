//
//  Enemy.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Enemy.h"

//
// Enemy: An rounded enemy that is capable of killing the hero
//
// Supported parameters:
//	patrolTime (float): the time it takes to go from left to right. Default: 0 (no movement)
//  patrolSpeed (float): the speed of the patrol (the speed is in Box2d units). Default: 2
//

@implementation Enemy
-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {
	
		frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"rat02.png"];
		[self setDisplayFrame:frame];

		// bodyNode properties
		reportContacts_ = BN_CONTACT_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_SPRITES_PNG;
		isTouchable_ = YES;
		
		// 1. destroy already created fixtures
		[self destroyAllFixturesFromBody:body];
		
		// 2. create new fixture
		b2FixtureDef	fd;
		b2CircleShape	shape;
		shape.m_radius = 0.5f;		// 1 meter of diameter (optimized size)
		fd.friction		= kPhysicsDefaultEnemyFriction;
		fd.density		= kPhysicsDefaultEnemyDensity;
		fd.restitution	= kPhysicsDefaultEnemyRestitution;
		fd.shape = &shape;
		
		// filtering... in case you want to avoid collisions between enemies
//		fd.filter.groupIndex = - kCollisionFilterGroupIndexEnemy;
		
		body->CreateFixture(&fd);
		body->SetType(b2_dynamicBody);	
        
        
        
        
        
		patrolActivated_ = NO;
		
		[self schedule:@selector(update:)];
        [self schedule:@selector(updateAnima:) interval:1];
        
	}
	return self;
}

-(void) setParameters:(NSDictionary *)params
{
	[super setParameters:params];
    NSString *patrolSpider = [params objectForKey:@"patrolSpider"];
    NSString *patrolFly = [params objectForKey:@"patrolFly"];
	
	NSString *patrolTime = [params objectForKey:@"patrolTime"];
	NSString *patrolSpeed = [params objectForKey:@"patrolSpeed"];
    
    NSString *enemyType = [params objectForKey:@"enemyType"];
    
	if(!patrolSpider &&  patrolTime ) {
		patrolTime_ = [patrolTime floatValue];
		
		patrolSpeed_ = 2; // default value
		if( patrolSpeed )
			patrolSpeed_ = [patrolSpeed floatValue];
        patrolActivated_ = YES;
        

        
	}
    
    if(patrolSpider){
    
        patrolTime_ = [patrolTime floatValue];
		
		patrolSpeed_ = 2; // default value
		if( patrolSpeed )
			patrolSpeed_ = [patrolSpeed floatValue];

        patrolSpiderActivated_ = YES;
        body_->SetGravityScale(0.0f);


    }
    if(patrolFly){
        patrolTime_ = [patrolTime floatValue];
		
		patrolSpeed_ = 2; // default value
		if( patrolSpeed )
			patrolSpeed_ = [patrolSpeed floatValue];
        
       // patrolSpiderActivated_ = YES;
        //patrolActivated_ = YES;
        patrolFlyActivated_ = YES;
        
        body_->SetGravityScale(0.0f);

    }
    
    
    
    enemyTypeString = nil;
    switch([enemyType intValue]){
        case 1:
            enemyTypeString = @"rat";
            break;
        case 2:
            enemyTypeString = @"spider";
            break;
        case 3:
            enemyTypeString = @"bat";
            break;
            
    }
    
    frameNumber =1;
    NSLog(@"%@",[NSString stringWithFormat:@"%@02.png",enemyTypeString]);
    
    frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@01.png",enemyTypeString]];
    [self setDisplayFrame:frame];
    
}
-(void) updateAnima:(ccTime)dt
{
    frameNumber++;
    if(frameNumber==5)frameNumber=1;
    frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"%@0%i.png",enemyTypeString,frameNumber]];
    [self setDisplayFrame:frame];
    
}
-(void) update:(ccTime)dt
{
	//
	// move the enemy if "patrol" is activated
	// In this example the enemy is moved using Box2d, and not cocos2d actions.
	//
	if( patrolActivated_ ) {
		patrolDT_ += dt;
		if( patrolDT_ >= patrolTime_ ) {
			patrolDT_ = 0;
			// This line eliminates the inertia
			body_->SetAngularVelocity(0);
			// Change the direction of the movement
			if( patrolDirectionLeft_ ) {
				body_->SetLinearVelocity( b2Vec2(-patrolSpeed_,0) );
			} else {
				body_->SetLinearVelocity( b2Vec2(patrolSpeed_,0) );
			}
			patrolDirectionLeft_ = ! patrolDirectionLeft_;
		}
	}
	
    if( patrolSpiderActivated_ ) {
		patrolDT_ += dt;
		if( patrolDT_ >= patrolTime_ ) {
            patrolDT_ = 0;
			// This line eliminates the inertia
			body_->SetAngularVelocity(0);
			// Change the direction of the movement
			if( patrolDirectionUp_ ) {
				body_->SetLinearVelocity( b2Vec2(0,-patrolSpeed_) );
			} else {
				body_->SetLinearVelocity( b2Vec2(0,patrolSpeed_) );
			}
			patrolDirectionUp_ = ! patrolDirectionUp_;
		}
	}
    if( patrolFlyActivated_ ) {
		patrolDT_ += dt;
		if( patrolDT_ >= patrolTime_ ) {
            patrolDT_ = 0;
			// This line eliminates the inertia
			body_->SetAngularVelocity(0);
			
			// Change the direction of the movement
			if( patrolDirectionUp_ ) {
				body_->SetLinearVelocity( b2Vec2(-patrolSpeed_,-patrolSpeed_) );
			} else {
				body_->SetLinearVelocity( b2Vec2(patrolSpeed_,patrolSpeed_) );
			}
			patrolDirectionUp_ = ! patrolDirectionUp_;
		}
	}
    
    
    
    //CHECK DISTANCE TO HERO
    /*
     NSLog(@"Testing distance");
     theDistance = (((self.position.x - [game_
     getHeroPosX])*(self.position.x - [game_ getHeroPosX]))+
     ((self.position.y - [game_ getHeroPosY])*(self.position.y -
     [game_ getHeroPosY])));
     if (sqrt(theDistance) < detectDistance){
     
     NSLog(@"Bingo !");
     }
     */
    
}

-(void) touchedByHero
{
        [game_ displayGameImageMessage:@"live0.png" positionX:self.position.x positionY:self.position.y];
        [[SimpleAudioEngine sharedEngine] playEffect:@"you_are_hit.wav"];
        [game_ increaseLife:-1];
}

-(void) hitByHero{
        // remove it
        [game_ displayGameImageMessage:@"points5.png" positionX:self.position.x positionY:self.position.y];
        [game_ removeB2Body:body_];
        [game_ increaseScore:5];
        [[SimpleAudioEngine sharedEngine] playEffect: @"coin1.wav"];
}

-(void) touchedByBullet:(id)bullet
{
    [game_ displayGameImageMessage:@"points10.png" positionX:self.position.x positionY:self.position.y];
	[game_ removeB2Body:body_];
	[[SimpleAudioEngine sharedEngine] playEffect: @"enemy_killed.wav"];
	[game_ increaseScore:10];

}

@end