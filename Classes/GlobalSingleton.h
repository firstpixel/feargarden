//
//  GlobalSingleton.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface GlobalSingleton : NSObject {
	NSUInteger board[2];  // c-style array
    
    int gameType;
	int gameKitState;
    int keyLocker;
    NSString* deviceType;
    NSString* princess;
    NSString* princessName;
    NSString* playerChar;
    int gamePoints;
    
    
    int bannerType;
    

}
@property (nonatomic, readwrite) int bannerType, gamePoints;
@property (nonatomic, retain) NSString *deviceType,*princess,*playerChar,*princessName;
@property (nonatomic, readwrite) int keyLocker;

@property (nonatomic, readwrite) int gameType; // 0 = SinglePlayer   1 = multiPlayer same Iphone   2 = multiPlayer bluetooth
@property (nonatomic, readwrite) int gameKitState; // 0 = Start Game   1 = Picker   2 = Multiplayer   3 = Multiplayer Coin Toss   4 = Multiplayer reconnect
@property (nonatomic, readwrite) BOOL audioOn;


+ (GlobalSingleton *) sharedInstance;
- (void)release;
-(void)unlockItem:(NSString*)item;
-(void)lockItem:(NSString*)item;
-(BOOL)isItemUnlocked:(NSString*)item;
-(void) clearData;


-(NSString*)timeToString:(int)t;

//
-(void)intPref:(int)integ forKey:(NSString*)key;
-(int)getIntPrefForKey:(NSString*)key;
//-(NSString*)getOpenFeintLeaderboardFromSelected;
//-(NSString*)getOpenFeintChallengeFromSelected;


-(int)addKey;
-(int)removeKey;

@end