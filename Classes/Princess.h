//
//  Princess.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 03/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "BodyNode.h"


@interface Princess : BodyNode {
    NSString* princessMessage;
    // elapsed time on the game
	ccTime				elapsedTime_;
    int blinkerAnima;
}

-(void) touchedByHero;
@end
