/*
 * Copyright (c) 2009-2011 Ricardo Quesada
 * Copyright (c) 2011-2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


//
// RootViewController + iAd
// If you want to support iAd, use this class as the controller of your iAd
//
@import GoogleMobileAds;

#import "cocos2d.h"
#import "RootViewController.h"
#import "GlobalSingleton.h"
#import "MKStoreKit/MKStoreKit.h"


@implementation RootViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
 [super viewDidLoad];
     bannerViewAdded = NO;
     [self addBanner];
     // Set GameCenter Manager Delegate
     //[[GameCenterManager sharedManager] setDelegate:self];
 }


- (void)addBanner{
    if(!bannerViewAdded && ![[MKStoreKit sharedKit] isProductPurchased:@"com.firstpixel.feargarden.full2"]){
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        
        bannerView = [[GADBannerView alloc]initWithAdSize:kGADAdSizeBanner];
        //kGADAdSizeBanner = 320x50
        //[bannerView setBackgroundColor:[UIColor blueColor]];
        
        bannerView.center = CGPointMake(size.width/2, 25);
        // Need to set this to no since we're creating this custom view.
        //bannerView.translatesAutoresizingMaskIntoConstraints = NO;
        
        // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
        // before compiling.
        
        // Replace this ad unit ID with your own ad unit ID.
        bannerView.adUnitID = @"ca-app-pub-4523467891811991/2519312296";
        bannerView.rootViewController = self;
        [self.view addSubview:bannerView];
        
        GADRequest *request = [GADRequest request];
        // Requests test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made. GADBannerView automatically returns test ads when running on a
        // simulator.
        request.testDevices = @[
                                @"2077ef9a63d2b398840261c8221a0c9a"  // Eric's iPod Touch
                                ];
        [bannerView loadRequest:request];
        bannerViewAdded = YES;
    }
    
}
- (void)removeBanner {
    if(bannerViewAdded){
        bannerViewAdded = NO;
        [bannerView removeFromSuperview];
        [bannerView release];
        bannerView = nil;
    }
}

 - (void)didReceiveMemoryWarning {
 [super didReceiveMemoryWarning];
 // Dispose of any resources that can be recreated.
 }



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	//
	//
	// return YES for the supported orientations
	
	// Only landscape ?
	return ( UIInterfaceOrientationIsLandscape( interfaceOrientation ) );
	
	// Only portrait ?
	//	return ( ! UIInterfaceOrientationIsLandscape( interfaceOrientation ) );
	
	// All orientations ?
	//  return YES;
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{	
}



- (void)viewDidUnload {
	[super viewDidUnload];
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[super dealloc];
}

@end
