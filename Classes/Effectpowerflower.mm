//
//  EffectPowerFlower.mm
//  LevelSVG
//
//  Created by Gil Beyruth on 10/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Effectpowerflower.h"

//
// Poison: An invisible, static object that can kill the hero.
// It is invisible because it should be drawn using a tiled map.
//

@implementation Effectpowerflower
-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {
		
		// bodyNode properties
		reportContacts_ = BN_CONTACT_NONE;
        properties_ = BN_PROPERTY_NONE;
		preferredParent_ = BN_PREFERRED_PARENT_EFFECTS;//BN_PREFERRED_PARENT_SPRITES_PNG BN_PREFERRED_PARENT_IGNORE
       isTouchable_ = NO;
        
        b2Fixture *fixture = body->GetFixtureList();
		fixture->SetSensor(true);
        
        id plantEffects = [CCParticleSystemQuad particleWithFile:@"plantEffects.plist"]; 
        [self addChild:plantEffects];

       	}
	return self;
}

@end
