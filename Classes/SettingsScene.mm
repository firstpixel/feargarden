//
//  SettingsScene.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 22/10/09.
//  Copyright 2009 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "SettingsScene.h"
#import "MenuScene.h"
#import "GameConfiguration.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
@implementation SettingsScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [SettingsScene node];
	
	[scene addChild:child];
	return scene;
}

-(id) init
{
	if( (self=[super init]) )
	{
		[super init];
		
        CGSize s = [[CCDirector sharedDirector] winSize];
		CCSprite *background = nil;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            background = [CCSprite spriteWithFile:@"main-menu-background-hd.png"];
        }else{
            background = [CCSprite spriteWithFile:@"main-menu-background.png"];
        }
        
		background.position = ccp(s.width/2, s.height/2);
		[self addChild:background z:-10];
        
        
        
        
        //Add menu to the screen
        SoundMenuItem *item0 = [SoundMenuItem itemFromNormalSpriteFrameName:@"accelerometer_off.png" selectedSpriteFrameName:@"accelerometer_on.png" target:self selector:@selector(controlAccelerometerCallback:)];	
		
        SoundMenuItem *item1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"joy_off.png" selectedSpriteFrameName:@"joy_on.png" target:self selector:@selector(controlJoyCallback:)];	
		
       
        GameConfiguration *config = [GameConfiguration sharedConfiguration];
        
        
		if( config.controlType == kControlTypeTilt ){
            
			[item0 selected];
         	[item1 unselected];
        }else if( config.controlType == kControlTypePad ){
            
			[item0 unselected];
         	[item1 selected];
        }
       
		CCMenu *menu1 = [CCMenu menuWithItems:
                         item0, item1,
                         nil];
		[menu1 alignItemsVerticallyWithPadding:34.0];
		[menu1 setPosition:ccp(s.width/2+30,s.height/2)];
		[self addChild: menu1];

        
        
		/*[CCMenuItemFont setFontName: @"Marker Felt"];
		[CCMenuItemFont setFontSize:34];
		CCMenuItemToggle *item1 = [CCMenuItemToggle itemWithTarget:self selector:@selector(controlCallback:) items:
								 [CCMenuItemFont itemFromString: @"Control: Tilt"],
								 [CCMenuItemFont itemFromString: @"Control: D-Pad"],
								 nil];
         CCMenuItemToggle *item2 = [CCMenuItemToggle itemWithTarget:self selector:@selector(controlFPS:) items:
								   [CCMenuItemFont itemFromString: @"Show FPS: OFF"],
								   [CCMenuItemFont itemFromString: @"Show FPS: ON"],
								   nil];

		CCMenuItemToggle *item3 = [CCMenuItemToggle itemWithTarget:self selector:@selector(controlWireframe:) items:
								   [CCMenuItemFont itemFromString: @"Wireframe: OFF"],
								   [CCMenuItemFont itemFromString: @"Wireframe: ON"],
								   nil];

		GameConfiguration *config = [GameConfiguration sharedConfiguration];
		if( config.controlType == kControlTypePad )
			item1.selectedIndex = 1;
		
		CCMenu *menu = [CCMenu menuWithItems:
					  item1, 
						nil];
		[menu alignItemsVertically];
		
		[self addChild: menu];
		
		*/

		
        //back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
        //backButton.position = ccp(15,s.height+20);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-100)];
            }
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else if([[GlobalSingleton sharedInstance] bannerType]==1){
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }else if([[GlobalSingleton sharedInstance] bannerType]==2){
                //iad
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-50)];
            }
        }
        
		backButton.anchorPoint = ccp(0,1);
		CCMenu *menu  = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        
       
		
		return self;
	}
	
	return self;
}

-(void) backCallback:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MenuScene scene] withColor:ccWHITE]];
}

-(void) controlFPS:(id)sender
{
	CCMenuItemToggle *item = (CCMenuItemToggle*) sender;	
	if( item.selectedIndex == 0 )
		[[CCDirector sharedDirector] setDisplayStats:NO];
	else
		[[CCDirector sharedDirector] setDisplayStats:YES];
}

-(void) controlWireframe:(id)sender
{
	CCMenuItemToggle *item = (CCMenuItemToggle*) sender;	
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
	if( item.selectedIndex == 0 )
		config.enableWireframe = NO;
	else
		config.enableWireframe = YES;
}

-(void) controlCallback:(id)sender
{
	CCMenuItemToggle *item = (CCMenuItemToggle*) sender;	
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
	if( item.selectedIndex == 0 )
		config.controlType = kControlTypeTilt;
	else
		config.controlType = kControlTypePad;
}

-(void) controlJoyCallback:(id)sender
{
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
    config.controlType = kControlTypePad;
        	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MenuScene scene] withColor:ccBLACK]];
}
-(void) controlAccelerometerCallback:(id)sender
{
	GameConfiguration *config = [GameConfiguration sharedConfiguration];
    config.controlType = kControlTypeTilt;
    	[[CCDirector sharedDirector] replaceScene: [CCTransitionFade transitionWithDuration:1 scene:[MenuScene scene] withColor:ccBLACK]];
}


@end
