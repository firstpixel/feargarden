//
//  Candy.mm
//  LevelSVG
//
//  Created by Gil Beyruth on 12/12/11.
//  Copyright 2010 Firstpixel. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import <Box2d/Box2D.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

#import "GameNode.h"
#import "GameConstants.h"
#import "Candy.h"


//
// Fruit: a sensor with the shape of an apple
// When touched, it will increase points in 1
//

@implementation Candy

-(id) initWithBody:(b2Body*)body game:(GameNode*)game
{
	if( (self=[super initWithBody:body game:game]) ) {

        int r = (arc4random() % 3)+1;
		CCSpriteFrame *frame = [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"candy%i.png",r]];
		[self setDisplayFrame:frame];

	}
	return self;
}

-(void) touchedByHero
{
    [game_ displayGameImageMessage:@"live5.png" positionX:self.position.x positionY:self.position.y];
	
	[super touchedByHero];
	[game_ increaseScore:1];
	[[SimpleAudioEngine sharedEngine] playEffect: @"coin1.wav"];
}
@end
