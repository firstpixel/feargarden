//
//  AboutNode.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "cocos2d.h"
#import "SimpleAudioEngine.h"


@interface IntroGameScene : CCLayer {
    
    SimpleAudioEngine *sae;
    CDSoundSource *backgroundLoop;

}
+(id) scene;

@end
