//
//  AboutNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "IntroGameScene.h"
#import "MenuScene.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"
#import "SelectCharScene.h"


@implementation IntroGameScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [IntroGameScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		
		CGSize size = [[CCDirector sharedDirector] winSize];
		CCSprite *background = nil;
        CCSprite *background2 = nil;
        background = [CCSprite spriteWithFile:@"background1.png"];
            background2 = [CCSprite spriteWithFile:@"background1.png"];
            CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"history.plist" textureFilename:@"history.pvr.ccz"];
        
        background.position = ccp(size.width, size.height/2);
		[self addChild:background];
        
        [background addChild:background2];
        [background2 setPosition:ccp(size.width+background.contentSize.width+10,size.height/2)];
        
        CCSprite *starring = [CCSprite spriteWithSpriteFrameName:@"starring-1.png"];
        starring.position = ccp(size.width/2+40, size.height/2+80);
        [background addChild:starring z:10];
        
         CCSprite *halloween = [CCSprite spriteWithSpriteFrameName:@"halloween.png"];
        halloween.position = ccp(size.width + size.width/2, size.height/2+80);
        [background addChild:halloween z:10];
        
        CCSprite *suddenlly = [CCSprite spriteWithSpriteFrameName:@"suddenlly.png"];
        suddenlly.position = ccp(size.width +size.width + size.width/2, size.height/2+50);
        [background addChild:suddenlly z:10];
      
        CCSprite *savethem = [CCSprite spriteWithSpriteFrameName:@"savethem.png"];
        savethem.position = ccp(size.width +size.width +size.width + size.width/2, size.height/2+80);
        [background addChild:savethem z:10];
        
        CCSequence *seq = [CCSequence actions:
                           [CCDelayTime actionWithDuration:4.0],
                           [CCMoveTo actionWithDuration:16.0 position:ccp(-(background.contentSize.width * 1.4) + size.width,size.height/2)],
                            nil];
        [background runAction:seq];
        //back button
        SoundMenuItem *backButton = [SoundMenuItem itemFromNormalSpriteFrameName:@"back_up.png" selectedSpriteFrameName:@"back_down.png" target:self selector:@selector(backCallback:)];	
        //backButton.position = ccp(15,s.height+20);
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-40)];
            }else{
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(30,s.height-130)];
            }
        }else{
            if([[GlobalSingleton sharedInstance] bannerType]==0){
                //none
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-20)];	
            }else{
                //admob
                CGSize s = [[CCDirector sharedDirector] winSize];
                [backButton setPosition:ccp(15,s.height-65)];
            }
        }
        
		backButton.anchorPoint = ccp(0,1);
		CCMenu* menu = [CCMenu menuWithItems:backButton, nil];
		menu.position = ccp(0,0);
		[self addChild: menu z:0];
        [self schedule:@selector(waitSecond:) interval:20];
       
        sae = [SimpleAudioEngine sharedEngine];
        [sae preloadEffect:@"Level2.mp3"];
        backgroundLoop = [[sae soundSourceForFile:@"Level2.mp3"] retain];
        backgroundLoop.looping = YES;
        backgroundLoop.gain = 0.6f;
        [backgroundLoop play];

	}
	return self;
}

-(void) waitSecond:(ccTime)dt
{
    [[GlobalSingleton sharedInstance] unlockItem:@"gameIntro"]; 
	[[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:1.0f scene:[SelectCharScene scene]]];
}

-(void) visitHomepageCallback: (id) sender {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.firstpixel.com/"]];
}

-(void) backCallback:(ccTime)dt
{
    [self unschedule:@selector(waitSecond:)]; 
	[[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:1.0f scene:[MenuScene scene]]];
}
@end
