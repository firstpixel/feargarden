//
//  SoundMenuItem.m
//  SapusTongue
//
//  Created by Ricardo Quesada on 17/09/08.
//  Copyright 2008 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//


#import "SoundMenuItemLabel.h"
#import "SimpleAudioEngine.h"

//
// A MeneItem that plays a sound each time is is pressed
// Added support for SpriteFrameNames
//
@implementation SoundMenuItemLabel

+(id) itemFromNormalSpriteFrameName:(NSString*)normalFrameName selectedSpriteFrameName:(NSString*)selectedFrameName target:(id)target selector:(SEL)selector
{
	return [[[self alloc] initFromNormalSpriteFrameName:normalFrameName selectedSpriteFrameName:selectedFrameName disabledSpriteFrameName:nil target:target selector:selector] autorelease];
}

+(id) itemFromNormalSpriteFrameName:(NSString*)normal selectedSpriteFrameName:(NSString*)selected disabledSpriteFrameName:(NSString*)disabled target:(id)target selector:(SEL)selector
{
	return [[[self alloc] initFromNormalSpriteFrameName:normal selectedSpriteFrameName:selected disabledSpriteFrameName:disabled target:target selector:selector] autorelease];
}


+(id) initFromNormalSpriteFrameName:(NSString*)normalFrameName selectedSpriteFrameName:(NSString*)selected label:(NSString*)labelName target:(id)target selector:(SEL)selector
{
	return [[[self alloc] initFromNormalSpriteFrameName:normalFrameName selectedSpriteFrameName:selected label:(NSString*)labelName target:target selector:selector] autorelease];
}

-(id) initFromNormalSpriteFrameName:(NSString*)normalFrameName selectedSpriteFrameName:(NSString*)selectedFrameName label:(NSString*)labelName target:(id)target selector:(SEL)selector
{
    if(self = [self initFromNormalSpriteFrameName:normalFrameName selectedSpriteFrameName:selectedFrameName disabledSpriteFrameName:nil target:target selector:selector]){
    
    CCLabelBMFont *menuLabel = [CCLabelBMFont labelWithString:labelName fntFile:@"comicw18-hd.fnt"];
    [menuLabel.texture setAliasTexParameters];
    [self addChild:menuLabel z:10];
    [menuLabel setAnchorPoint:ccp(0.5f,1)];
    [menuLabel setPosition:ccp(30, 15)];
    
    }
    
    
    return self;
}

-(id) initFromNormalSpriteFrameName:(NSString*)normalFrameName selectedSpriteFrameName:(NSString*)selectedFrameName target:(id)target selector:(SEL)selector
{
    
    return [self initFromNormalSpriteFrameName:normalFrameName selectedSpriteFrameName:selectedFrameName disabledSpriteFrameName:nil target:target selector:selector];
}


-(id) initFromNormalSpriteFrameName:(NSString*)normalFrameName selectedSpriteFrameName:(NSString*)selectedFrameName disabledSpriteFrameName:(NSString*)disabledFrameName target:(id)target selector:(SEL)selector
{
	CCSprite *normal = [CCSprite spriteWithSpriteFrameName:normalFrameName];
	CCSprite *selected = [CCSprite spriteWithSpriteFrameName:selectedFrameName];
	
	CCSprite *disabled = nil;
	if( disabledFrameName )
		disabled = [CCSprite spriteWithSpriteFrameName:disabledFrameName];
				
	if( (self=[super initFromNormalSprite:normal selectedSprite:selected disabledSprite:disabled target:target selector:selector]))
	{

		// nothing
	}
	return self;	
}

-(void) selected {
	[super selected];

	[[SimpleAudioEngine sharedEngine] playEffect:@"snd-tap-button.caf"];
}

@end
