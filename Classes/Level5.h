//
//  Level1.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 06/01/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "GameNode.h"
#import "SimpleAudioEngine.h"
#import "GlobalSingleton.h"
@interface Level5 : GameNode
{
	// Sprite batch node weak ref
	CCSpriteBatchNode	*spritesBatchNode_;
	CCSpriteBatchNode	*platformBatchNode_;
	CCSpriteBatchNode	*invisibleBatchNode_;
    
    
    SimpleAudioEngine *sae;
    CDSoundSource *backgroundLoop;
    
}

@end
